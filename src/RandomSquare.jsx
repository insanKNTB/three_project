import {useEffect, useRef} from "react";
import * as Three from "three";
import "./styles.css"

const App = () => {
  const contentRef = useRef()
  const scene = new Three.Scene();
  let listSquare = []

  scene.background = new Three.Color(0xF9F36D)

  const camera = new Three.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
  const renderer = new Three.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);

  camera.position.set(0, 0, 40);
  camera.lookAt(0, 0, 0)

  const getRandom = (min, max) => Math.floor(Math.random() * (max - min)) + min;

  const randomColor = () => Math.floor(Math.random()* 16777215).toString(16);

  function animate() {
    requestAnimationFrame(animate);

    listSquare.forEach(item => {
      item.rotation.x += 0.01;
      item.rotation.y += 0.01;
    })

    renderer.render(scene, camera);
  }

  const generateSquare = () => {
    const sizeSquare = getRandom(1, 11);

    const geometry = new Three.BoxGeometry(sizeSquare, sizeSquare, sizeSquare);

    const material = new Three.MeshBasicMaterial({color: parseInt("0x" + randomColor(), 16)  });

    const box3D = new Three.Mesh(geometry, material);

    const positionX = getRandom(-25, 25);
    const positionY = getRandom(-25, 25);
    const positionZ = getRandom(-25, 25);

    box3D.position.set(positionX, positionY, positionZ)

    return box3D
  }

  const generateScene = () => {
    listSquare = []

    for (let i = 0; i <= 30; i++) listSquare.push(generateSquare());

    listSquare.forEach(item => scene.add(item));
  }

  const updateScene = () => {
    scene.clear();

    generateScene();
  }

  const cleanScene = () => {
    scene.clear();
  }

  useEffect(() => {
    const {current} = contentRef;

    current.append(renderer.domElement);

    generateScene();

    animate();
  }, []);

  return (
    <div>
      <div style={{position: 'fixed', left: 0, top: 0}}>
        <button onClick={updateScene}>Update</button>
        <button onClick={cleanScene}>Clean</button>
      </div>

      <div ref={contentRef}></div>
    </div>
  );
};

export default App;