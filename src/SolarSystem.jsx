import {useEffect, useRef} from "react";
import * as Three from "three"

import {OrbitControls} from 'three/addons/controls/OrbitControls.js';

const SolarSystem = () => {
  const contentRef = useRef()
  const controlRef = useRef()
  
  let controls = {}
  const listSphere = []
  const scene = new Three.Scene;
  const renderer = new Three.WebGLRenderer({antialias: true});
  renderer.shadowMapEnabled = true;
  renderer.shadowMapSoft = true;
  const camera = new Three.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000);
  
  renderer.shadowMap.enabled = true
  
  renderer.setSize(window.innerWidth, window.innerHeight);
  scene.background = new Three.Color(0x000000)
  camera.position.set(0, 0, 120);
  
  camera.lookAt(0, 0, 0)
  
  function moveBlock(sphere) {
    const x = Math.cos(sphere?.angle) * sphere?.radius;
    const y = Math.sin(sphere?.angle) * sphere?.radius;
    
    if (sphere?.isRight) {
      sphere.angle += sphere?.speed;
    } else {
      sphere.angle -= sphere?.speed;
    }
    
    return {x, y};
  }
  
  const light = new Three.PointLight(0xfffffff, 1, 100);
  light.position.set(50, 50, 50);
  scene.add(light);
  
  const generateSphere = (radius, props) => {
    const geometry = new Three.SphereGeometry(radius);
    const material = new Three.MeshPhongMaterial(props);
    
    return new Three.Mesh(geometry, material);
  }
  
  const generateSphereWithPosition = (style, radius, x = 0, y = 0, radiusSphere = 0, angle = 0, isRight = true, speed = 0.01, z = 10) => {
    const sphereGenerate = generateSphere(radius, style);
    
    sphereGenerate.radius = radiusSphere;
    sphereGenerate.angle = angle;
    sphereGenerate.isRight = isRight;
    sphereGenerate.position.set(x, y, 0);
    sphereGenerate.speed = speed;
    sphereGenerate.castShadow = true;
    sphereGenerate.receiveShadow = true;
    
    scene.add(sphereGenerate);
    listSphere.push(sphereGenerate);
  }
  
  
  useEffect(() => {
    const {current} = contentRef;
    
    current.append(renderer.domElement);
    
    animate();
    
    controlRef.current
    
    controls = new OrbitControls(camera, renderer.domElement);
    controls.target.set(0, 0, 0);
    controls.update();
  }, []);
  
  const animate = () => {
    requestAnimationFrame(animate);
    
    listSphere.forEach(item => {
      const {x, y} = moveBlock(item);
      
      item.position.x = x;
      item.position.y = y;
    })
    
    renderer.render(scene, camera);
  }
  
  window.addEventListener('resize', onWindowResize);
  
  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    
    renderer.setSize(window.innerWidth, window.innerHeight);
  }
  
  const mainSphere = generateSphere(7, {
    shininess: 100, side: Three.BackSide
  })
  
  const sunLight = new Three.PointLight(0xFFE002, 800, 700);
  sunLight.castShadow = true;
  sunLight.shadow.bias = -0.005;
  
  const onReset = () => {
    camera.position.set(0, 0, 120);
    camera.lookAt(0, 0, 0)
  }
  
  mainSphere.add(sunLight)
  
  scene.add(mainSphere)
  
  generateSphereWithPosition({color: 0x1884E4}, 2, 16, 16, 16, 0, true, 0.06)
  
  generateSphereWithPosition({color: 0x1884E4}, 3, 26, -26, 26, 40, true, 0.03)
  
  generateSphereWithPosition({color: 0x1884E4}, 3.3, 36, 36, 36, -20, true, 0.01)
  
  generateSphereWithPosition({color: 0x1884E4}, 2.2, 46, 46, 46, 80, false, 0.005)
  
  generateSphereWithPosition({color: 0x1884E4}, 4, 56, 56, 56, -12, true, 0.001)
  
  generateSphereWithPosition({color: 0x1884E4}, 3.4, 68, 68, 68, 60, true, 0.0005)
  
  generateSphereWithPosition({color: 0x1884E4}, 2, 78, 78, 78, -45, false, 0.0002)
  
  generateSphereWithPosition({color: 0x1884E4}, 2, 86, 86, 86, -30, true, 0.00005)
  
  const planeGeometry = new Three.PlaneGeometry(innerWidth, innerHeight)
  
  const material = new Three.MeshPhongMaterial({
    color: 0xa0adaf,
  });
  
  const meshPlane = new Three.Mesh( planeGeometry, material );
  
  meshPlane.position.z = -8;
  meshPlane.receiveShadow = true;
  
  scene.add(meshPlane);
  
  return (
    <div>
      <div style={{position: "fixed", top: 0, left: 0}}>
        <button style={{cursor: "pointer"}} onClick={onReset}>Reset</button>
      </div>
      
      <div ref={contentRef}></div>
    </div>
  );
};

export default SolarSystem;