import RandomSquare from "./RandomSquare.jsx";
import SolarSystem from "./SolarSystem.jsx";
import Sphere from "./Sphere.jsx"

const App = () => {
  return (
    <div>
      {/*<RandomSquare />*/}
      {/*<SolarSystem />*/}
      <Sphere />
    </div>
  );
};

export default App;