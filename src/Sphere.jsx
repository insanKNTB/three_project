import React, {useEffect, useRef} from 'react';
import {OrbitControls} from 'three/addons/controls/OrbitControls.js';

import * as Three from "three";

const Sphere = () => {
  const centerSphere = useRef()
  const contentRef = useRef();
  
  const scene = new Three.Scene;
  const renderer = new Three.WebGLRenderer();
  renderer.shadowMapEnabled = true;
  renderer.shadowMapSoft = true;
  const camera = new Three.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000);
  
  renderer.shadowMap.enabled = true;
  renderer.setSize(window.innerWidth, window.innerHeight);
  
  scene.background = new Three.Color(0x000000)
  
  camera.position.set(0, 0, 120);
  camera.lookAt(0, 0, 0)
  
  const animate = () => {
    requestAnimationFrame(animate);
    
    const {current: sphereCenter} = centerSphere
    
    if (sphereCenter.position.z <= 11 && sphereCenter.isRight) {
      sphereCenter.position.z += 0.1
      
      if (sphereCenter.position.z >= 10) {
        sphereCenter.isRight = false;
      }
    }
    
    if(sphereCenter.position.z > -11 && !sphereCenter.isRight){
      sphereCenter.position.z -= 0.1;
      
      if(sphereCenter.position.z <= -10){
        sphereCenter.isRight = true;
      }
    }
    
    if (sphereCenter.position.x <= 11 && sphereCenter.isX) {
      sphereCenter.position.x += 0.2
      
      if (sphereCenter.position.x >= 10) {
        sphereCenter.isX = false;
      }
    }
    
    if(sphereCenter.position.x > -11 && !sphereCenter.isX){
      sphereCenter.position.x -= 0.2;
      
      if(sphereCenter.position.x <= -10){
        sphereCenter.isX = true;
      }
    }
    
    
    if (sphereCenter.position.y <= 11 && sphereCenter.isY) {
      sphereCenter.position.y += 0.4;
      
      if (sphereCenter.position.y >= 10) {
        sphereCenter.isY = false;
      }
    }
    
    if(sphereCenter.position.y > -11 && !sphereCenter.isY){
      sphereCenter.position.y -= 0.4;
      
      if(sphereCenter.position.y <= -10){
        sphereCenter.isY = true;
      }
    }
    
    renderer.render(scene, camera);
  }
  
  const sphereCenter = () => {
    const geometrySphere = new Three.SphereGeometry(5);
    const material = new Three.MeshBasicMaterial({color: 0xffffff});
    
    const sphere3D = new Three.Mesh(geometrySphere, material);
    
    sphere3D.position.set(0, 0, 0);
    
    const sunLight = new Three.PointLight(0xFFFFFF, 200, 200);
    sunLight.castShadow = true;
    sunLight.shadow.bias = -0.005;
    
    sphere3D.add(sunLight);
    
    sphere3D.isRight = true;
    sphere3D.isX = true;
    sphere3D.isY = true;
    
    centerSphere.current = sphere3D;
    
    scene.add(sphere3D)
  }
  
  const boxCenter = () => {
    const geometryBox = new Three.BoxGeometry(40, 40, 40);
    
    const material = new Three.MeshPhongMaterial({
      side: Three.BackSide
    });
    
    const box3D = new Three.Mesh(geometryBox, material);
    
    box3D.position.set(0, 0, 0);
    
    scene.add(box3D);
  }
  
  
  useEffect(() => {
    const {current} = contentRef;
    
    current.append(renderer.domElement);
    
    sphereCenter();
    boxCenter();
    
    animate()
    
    renderer.render(scene, camera);
    
    const controls = new OrbitControls(camera, renderer.domElement);
    controls.target.set(0, 0, 0);
    controls.update();
  }, [])
  
  return (
    <div ref={contentRef}>
    
    </div>
  );
};

export default Sphere;